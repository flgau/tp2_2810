#include "Devineur.h"
#include "ErreurMotDevine.h"

void Devineur::afficherNombreErreurs(AutomateReponse reponse, string essai){
	int nbErreurs = reponse.compterErreur(essai);
	incrementerNbEssai();
	if (nbErreurs == 0) {
		
		cout << "Felicitations vous avez trouve le code en " << nbEssais << " essai";
		if (nbEssais > 1)
			cout << 's';
		cout << "!\n";
		resetNbEssai();
		throw FinDeJeuException();

	}
	if(nbErreurs == 1)
		cout << "Oh Non! Il y a " << nbErreurs << " erreur dans votre essai. Veuillez reessayer\n";
	else
		cout << "Oh Non! Il y a " << nbErreurs << " erreurs dans votre essai. Veuillez reessayer\n";
	if (nbEssais == 15) {
		throw TropDErreur();
	}

	
}

void Devineur::jouer(AutomateReponse automateReponse)
{
	
	try
	{
		while (true) {
			cout << "Entrer votre essai pour deviner le code secret. Indice, le mot a "<< automateReponse.getNbLettreReponse() <<" lettres : ";

			string essai;
			cin >> essai;
			if(essai.size() == automateReponse.getNbLettreReponse())
				afficherNombreErreurs(automateReponse, essai);
			else {
				
				cout << "Entrer une combinaison avec le meme nombre de lettre!\n";
			}

		}

	}
	catch (const FinDeJeuException&)
	{
		//rien a faire le jeu est fini
		return;
	}
	catch (const TropDErreur&) {
		cout << "Fin du jeu. Vous avez epuise vos tentatives. Le mot etait ";
		automateReponse.afficherReponse();
		cout << '\n';

	}
	

}

