#pragma once
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>

using namespace std;

class Sommet {

public:

	Sommet(char let) {

		lettre = let;
		estFinale = false;
	}

	Sommet() { lettre = ' '; estFinale = false; }

	void setEstFinale(bool b) {

		this->estFinale = true;
	}

	bool getEstFinale() {

		return this->estFinale;
	}

	vector<Sommet*>* getListeFils() {

		return &fils;
	}

	char getLettre() {

		return lettre;
	}


private:
	vector<Sommet*> fils; 
	char lettre;
	bool estFinale;
};

