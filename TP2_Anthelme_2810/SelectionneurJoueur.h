#pragma once


#include "SelectionneurOrdi.h"

class SelectionneurJoueur : public SelectionneurAbstrait {
public:
	SelectionneurJoueur() = default;

	std::string selectionnerMot(Automate automate) override;
};






//le joueur marque bbbba. 
//Verifier si bbbba est dans le lexique
//Si oui : le mot est choisi
//	On demande si cest le mot voulu
//	Si oui on cree l'automate reponse
//Si non : on affiche les suggestions