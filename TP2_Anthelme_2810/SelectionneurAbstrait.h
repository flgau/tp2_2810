#include <string>
#include "Automate.h"
class SelectionneurAbstrait {
public:
	SelectionneurAbstrait() = default;
	virtual std::string selectionnerMot(Automate automate) = 0;

private:

};
