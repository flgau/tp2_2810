#pragma once
#include <string>
#include <iostream>
#include "SommetReponse.h"


class AutomateReponse
{
public:

	AutomateReponse(std::string reponse);
	int compterErreur(std::string essai) const;
	SommetReponse getRacine() { return racine; }
	void afficherReponse() { std::cout << reponse; }
	int getNbLettreReponse() { return reponse.size(); }
	





private:
	std::string reponse;
	SommetReponse racine;
	void creerVerif(std::string reponse);
	void ajouterLettre(const char& lettreAAjouter, SommetReponse* sommetOuAjouter, bool estFinal);

	
};


