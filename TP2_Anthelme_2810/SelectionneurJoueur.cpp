#include "SelectionneurJoueur.h"
#include <algorithm>



std::string SelectionneurJoueur::selectionnerMot(Automate automate)
{
	string motChoisi;
	cout << "Veuiller choisir un mot : ";
	cin >> motChoisi;
	cout << '\n';

verifierMot:
		vector<string> lesSuggestions = automate.recuperSuggestions(motChoisi);
		while (std::find(lesSuggestions.begin(), lesSuggestions.end(), motChoisi) == lesSuggestions.end()) {
			cout << "Le mot n'est pas valide veuillez en choisir un parmi les suivants : \n";
			automate.afficherSuggestion(motChoisi);
			cout << "\nEntrer le mot que vous voulez : " ;
			cin >> motChoisi;
			lesSuggestions = automate.recuperSuggestions(motChoisi);
		}
	cout << "Le mot choisi est valide. Voici les autres suggestions : \n";
	automate.afficherSuggestion(motChoisi);
ValiderMot:
	cout << "Voulez vous choisir le mot que vous avez inscrit? Y:N : ";
	char reponse;
	
	cin >> reponse;

	if (reponse == 'Y')
		goto motEstChoisi;
	else if(reponse == 'N')
		goto rechoisirMot;
	else {
		cout << "L'option entree n'est pas valide pour dire que vous voulez valider le mot vous devez entrer Y,\npour rechoisir vous devez entrer N" << endl;
		goto ValiderMot;
	}

rechoisirMot:
	cout << "Entrer le mot que vous voulez choisir : ";
	cin >> motChoisi;
	goto verifierMot;


motEstChoisi:
	system("CLS");
	cout << "Votre reponse a ete cache veuillez passer lordinateur au 2e joueur. \n\n\n";
	return motChoisi;


}