﻿#include "Automate.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <random>

Automate::Automate()
{
	this->racine = new Sommet('*');
	this->nbrMot = 0;
}

Automate::Automate(string nomFichierTxt) {

	/*racine = new Sommet('*');
	ifstream fichier;
	fichier.open(nomFichierTxt);
	string mot;
	this->nbrMot = 0;

	if (fichier.is_open()) {

		while (getline(fichier, mot)) {

			ajouterMot(mot);
			this->nbrMot += 1;
		}
	}
	else {
		cout << "chemin du fichier incorrecte" << endl;
	}*/
	creerLexique(nomFichierTxt);

}
void Automate::creerLexique(string nomFichierTxt)
{
	racine = new Sommet('*');
	ifstream fichier;
	fichier.open(nomFichierTxt);
	string mot;
	this->nbrMot = 0;

	if (fichier.is_open()) {

		while (getline(fichier, mot)) {

			ajouterMot(mot);
			this->nbrMot += 1;
		}
	}
	else {
		cout << "chemin du fichier incorrecte" << endl;
	}
}


void Automate::ajouterMot(string mot) {

	ajouterLettres(mot, this->racine, 0);
}

void Automate::afficher(Sommet* sommet, string str)
{
	if (sommet == NULL) {
		sommet = racine;
	}

	if (sommet->getEstFinale()) {

		cout << str << endl;

	}

	for (Sommet* s : *sommet->getListeFils()) {

		afficher(s, (str + s->getLettre()));
	}

}


void Automate::ajouterLettres(string mot, Sommet* sommet, unsigned int index) {
	bool trouve = false;
	for (Sommet* s : *sommet->getListeFils()) {

		if (mot[index] == s->getLettre()) {
			trouve = true;

			//si on a pas fini de verifier la presence du mot et qu'on est pas sur une feuille
			//ici, si nous sommes sur une feuille, c'est qu'il y a un plus petit mot commencant pareil, on ne veut donc pas l'�craser
			if (index < (mot.size() - 1)) {

				if (s->getListeFils()->size() > 0 || s->getEstFinale()) {
					ajouterLettres(mot, s, index + 1); //on rappelle la fonction avec la prochaine lettre et le nouveau sommet
				}
				//on a trouver la m�me lettre mais elle sert a finir un autre mot, donc on ne peut pas l'utiliser sinon on efface l'autre
				else {
					trouve = false;
				}
			}
			//si derniere lettre
			else if (index == (mot.size() - 1) && s->getEstFinale()) {

				return; //le mot a ete trouvee
			}
			//si le mot est trouve mais la derniere lettre n'est pas finale
			else if (s->getEstFinale() == false) {
				s->setEstFinale(true);
				this->taille++;
				return;
			}
		}
	}
	//si pas trouve la lettre
	if (trouve == false) {

		Sommet* nouveauxSommet = new Sommet(mot[index]);
		sommet->getListeFils()->push_back(nouveauxSommet);
		if (index < (mot.size() - 1)) {

			ajouterLettres(mot, nouveauxSommet, index + 1); //on rappelle la fonction avec la prochaine lettre et le nouveau sommet
		}
		else {
			nouveauxSommet->setEstFinale(true);
			this->taille++;
		}
	}
}

unsigned int Automate::getNombreFeuilles(Sommet* sommet) {

	if (sommet == NULL) {
		sommet = racine;
	}


	if (sommet->getListeFils()->size() == 0) {

		return 1;
	}
	else {
		int taille = 0;
		for (Sommet* s : *sommet->getListeFils()) {

			taille += getNombreFeuilles(s);
		}

		return taille;
	}
	return 0;
}

void Automate::afficherSuggestion(string motEntre)
{
	vector<string> sugg = trouverSuggestions(racine, motEntre, 0, "");
	if (sugg.size() != 0) {
		for (string mot : sugg) {

			cout << mot << endl;
		}
	}
	else
		cout << "Il n'y a aucune autre suggestion pour le mot que vous avez choisi\n";
}

vector<string> Automate::trouverSuggestions(Sommet* sommet, string motEntre, int index, string prefix) {

	Sommet* sommetSuggestions = NULL;
	for (Sommet* s : *sommet->getListeFils()) {

		if (motEntre[index] == s->getLettre()) {

			if (index == motEntre.size() - 1) {

				sommetSuggestions = s;
			}
			else
				return trouverSuggestions(s, motEntre, index + 1, prefix + s->getLettre());
		}
	}

	vector<string>* mots = new vector<string>();

	if (sommetSuggestions != NULL) {

		trouverMots(sommetSuggestions, mots, prefix + sommetSuggestions->getLettre());
		return *mots;
	}
	else
		return *mots;

}

void Automate::trouverMots(Sommet* sommet, vector<string>* mots, string prefix)
{
	if (sommet->getEstFinale()) {

		mots->push_back(prefix);
	}

	for (Sommet* s : *sommet->getListeFils()) {

		trouverMots(s, mots, prefix + s->getLettre());
	}
}

string Automate::choisirMot() {

	return choisirMotHasard(this->racine, "");
}


string Automate::choisirMotHasard(Sommet* s, string str)
{

	if (s->getLettre() != '*')
		str += s->getLettre();

	if (s->getEstFinale()) {

		if (s->getListeFils()->size() == 0 || !doitContinuer()) {

			return str;
		}
	}

	int index = getRandom(s->getListeFils());

	Sommet* sommet = s->getListeFils()->at(index);

	return choisirMotHasard(sommet, str);

}

int Automate::getRandom(vector<Sommet*>* enfants) {

	// from https://docs.microsoft.com/en-us/cpp/standard-library/random?view=msvc-160

	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> dist(0, enfants->size() - 1);


	return dist(gen);
}

bool Automate::doitContinuer()
{
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> dist(0, 4);

	return dist(gen) != 0;
}





vector<string> Automate::recuperSuggestions(string motEntre)
{
	return trouverSuggestions(racine, motEntre, 0, "");
	
}

