#include "AutomateReponse.h"

AutomateReponse::AutomateReponse(std::string reponse)
{
	this->reponse = reponse;
	SommetReponse sommetReponse;
	racine = sommetReponse;
	creerVerif(reponse);
	
}

void AutomateReponse::creerVerif(std::string reponse){
	SommetReponse* tete = &racine;
	for (unsigned int i = 0; i < reponse.size(); i++) {
		char lettreAAjouter = reponse[i];
		bool estFinal = (i == reponse.size() - 1);
		ajouterLettre(lettreAAjouter, tete, estFinal);
		if (!estFinal) {
			tete->enfant = new SommetReponse;
			tete = tete->enfant;
		}
	}
}

void AutomateReponse::ajouterLettre(const char& lettreAAjouter, SommetReponse* sommetOuAjouter, bool estFinal){
	sommetOuAjouter->bonneReponse = lettreAAjouter;
	sommetOuAjouter->estFinal = estFinal;
}



int AutomateReponse::compterErreur(std::string essai) const{
	int nbErreurs = 0;
	SommetReponse tete = racine;
	for (char lettre : essai) {
		if (lettre != tete.bonneReponse) {
			nbErreurs++;
		}
		if (!tete.estFinal) {
			tete = *tete.enfant;
		}
		else 
			return nbErreurs;
	}
	return nbErreurs;

	
}


