#include <iostream>
#include "Automate.h"
#include "AutomateReponse.h"
#include <exception>

#include "SelectionneurJoueur.h"
#include "Devineur.h"
const string LEXIQUE_NON_DEFINI = "non defini";
void initialiserJeu(string& adresseLexique);
void modeAuto(string adresseLexique);
void modeVersus(string adresseLexique);

int main() {

	string nomLexique = LEXIQUE_NON_DEFINI;

	cout << "Bienvenue au jeu Code Master!\n";
	cout << "Voici les options : \n";
affichageInterface:
	cout << "1 : Initialiser Jeu.\n";
	cout << "2 : Mode Auto.\n";
	cout << "3 : Mode Versus.\n";
	cout << "4 : Quitter.\n";
	cout << "Entrer votre choix : [1, 2, 3, 4] : ";

	char choix = '0';
	cin >> choix;
	switch (choix)
	{
	case '1':
		initialiserJeu(nomLexique);
		break;
	case '2':
		modeAuto(nomLexique);
		break;
	case '3': 
		modeVersus(nomLexique);
		break;
	case '4':
		//quitter le programme
		cout << "A la prochaine fois!";
		return 0;

	default:
		cout << "Entree invalide, veuillez entrer un choix valide. Les choix sont 1, 2, 3 et 4.\n";
		
		goto affichageInterface;
	}
	cout << "\n";
	goto affichageInterface;
}




void modeVersus(string adresseLexique) {
	
	Devineur devineur;
	if (adresseLexique == LEXIQUE_NON_DEFINI) {
		cout << "Vous devez d'abord initialiser le jeu!\n";
		return;
	}
	SelectionneurAbstrait* selectionneur = new SelectionneurJoueur;
	devineur.jouer(AutomateReponse(selectionneur->selectionnerMot(Automate(adresseLexique))));

}
void modeAuto(string adresseLexique){
	Devineur devineur;
	if (adresseLexique == LEXIQUE_NON_DEFINI) {
		cout << "Vous devez d'abord initialiser le jeu!\n";
		return;
	}
	SelectionneurAbstrait* selectionneur = new SelectionneurOrdi;
	devineur.jouer(AutomateReponse(selectionneur->selectionnerMot(Automate(adresseLexique))));
}

void initialiserJeu(string& adresseLexique) {

	cout << "Quelle est l'adresse du lexique : ";
	cin >> adresseLexique;
	
	while (!ifstream(adresseLexique).good()) {
		cout << "\nL'adresse du fichier n'est pas bonne veuiller reessayer : ";
		cin >> adresseLexique;
	}
	cout << "Le jeu a bien ete initialise! Vous pouvez maintenant jouer!\n";
	return;
}

