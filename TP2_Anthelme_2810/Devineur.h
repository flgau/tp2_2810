#pragma once
#include "AutomateReponse.h"
#include "SelectionneurJoueur.h"
#include <stdlib.h>


class Devineur {
public:
	Devineur() :nbEssais(0) {}
	void afficherNombreErreurs(AutomateReponse reponse, string essai);
	void jouer(AutomateReponse reponse);

private:
	int nbEssais;
	void incrementerNbEssai() { nbEssais++; }
	void resetNbEssai() { nbEssais = 0; }


	

};