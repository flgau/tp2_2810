#pragma once
#include "sommet.h"


class Automate {

public:
	Automate();
	Automate(string nomFichierTxt);


	void ajouterLettres(string mot, Sommet* sommet, unsigned int index);
	void ajouterMot(string mot);
	void afficher(Sommet* sommet, string str);
	unsigned int getNombreFeuilles(Sommet* s);
	void afficherSuggestion(string motEntre);
	vector<string> trouverSuggestions(Sommet* s, string motEntre, int index, string prefix);
	void trouverMots(Sommet* s, vector<string>* mots, string prefix);
	int nbrMot;
	int taille = 0;
	string choisirMot();
	string choisirMotHasard(Sommet* s, string str);
	int getRandom(vector<Sommet*>*);
	bool doitContinuer();
	vector<string> recuperSuggestions(string motEntre);

private:
	void creerLexique(string nomFichierTxt);
	Sommet* racine;
};